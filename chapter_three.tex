\chapter{Presentation of the novel \textit{Marta}}
This chapter describes the novel \textit{Marta} by Eliza Orzeszkowa.
It contains a brief presentation of the novel's historical background: the Polish people's struggle for independence, the literary trend called ``Warsaw Positivism,'' and the life story of the author.
Finally, the plot of the novel is outlined.

\section{Historical background of the novel}

In order to fully understand the works of Eliza Orzeszkowa, it is important to put her writings in their historical context.
Towards the end of the 18th century, the Polish-Lithuanian Commonwealth was in decline.
Taking advantage of the weaknesses of the Polish state and military, three neighboring countries gradually annexed the territory of the Commonwealth.
Three partitions of Poland have taken place, in 1772, 1793, and 1795, after which the Polish crown lost all territory to the Russian Empire, the Kingdom of Prussia, and the Austro-Hungarian Empire, and Poland ceased to exist as a country.
After the partitions, the ruling forces exercised policies aiming to uproot any expression of Polish patriotism and nationalism.
These policies have been particularly strict in the Russian and Prussian partitions.

Unfortunately for the occupants, the Polish people were not willing to give up their patriotic spirit.
For 123 years, between 1795 and 1918, their struggle for independence constituted an important topic in Polish-language literature.
After the failure of the 1863 January Uprising against the Russian Empire, the Polish people were disappointed with Romanticism and slogans of armed fight for independence
% The fallen uprising and the Polish question had also been an important topic for European socialists
(Stekloff 1928).

The end of the January Uprising is considered to be the beginning of a literary and philosophical genre known as ``Warsaw Positivism'' (French: \textit{positivisme varsovien}, Polish: \textit{pozytywizm warszawski}).
This philosophy emphasized the importance of the so-called ``organic work'' (Polish: \textit{praca organiczna}), that is, active development of education and economy.
% It was particularly important in the works of Positivist writers, who believed that the key to independence was education and hard work, rather than uprisings and revolutions.
The key topics in Polish Positivist literature included the fight for independence, the emancipation of serfs and women, promoting science, medicine and public hygiene, and the assimilation of the Jewish minority with the Polish society.
Besides Eliza Oreszkowa, important representants of Warsaw Positivism included Bolesław Prus (1847--1912), Maria Konopnicka (1842--1910), and Henryk Sienkiewicz (1846--1916), the laureate of the Nobel Prize in Literature 1905
(eSzkola.pl).

Gloger (2007) states that in the case of Poland, Positivism played a similar role to that of Enlightenment in Western Europe, that is to say, it paved ground for the development of modernism and modernity, popularizing rationalism and a scientific approach to reality. On the other hand, the cultural impact of Enlightenment in Poland had been rather limited due to the overall civilizational lag and unfavorable historical circumstances.

\section{The life and times of Eliza Orzeszkowa}

Eliza Orzeszkowa was born Eliza Pawłowska on June 6th, 1841, in the village of Milkowszczyzna, north of the Niemen river, in present-day Belarus, to a family of gentry.
Her father, Benedykt Pawłowski, died when she was three years old. 
At the age of ten, she moved to Warsaw, the present-day capital of Poland, where for the following five years she attended a boarding school, run by the nuns of the Order of the Holy Sacrament.
During that period, she had studied French, German, and Polish literature.

In 1858, at the age of 17, her parents arranged her marriage with a wealthy landowner by the name of Piotr Orzeszko.
In her diaries, Eliza admits her fondness for the tall, blond, and handsome man, and was excited to leave her family home to explore the country and to visit the man's numerous relatives, living in different parts of the Russian empire.
At the day of their wedding, the bridegroom was 35 years old
(Shastouski).

The marriage of Mr. and Mrs. Orzeszko proved unsuccessful, mainly due to Eliza's political interests and affiliations---Eliza was highly pro-independence and sought the emancipation of serfs.
Her husband, on the other hand, as a member of the bourgeoisie, was more inclined to protect the interests of his class, which saw the political turmoil brought about by military activities as an opportunity to further accumulate wealth
(Britannica, Bachórz, Brykowisko, 2011, Orzeszkowa, 2000, p. 7).

% In 1864, serfdom was effectively abolished in the Russian partition --- peasants were allowed to own their land.

During the January Uprising, Eliza was actively working for the Polish cause, passing messages between the troops, and even helping Mr. Romuald Traugutt, the leader of the insurrection from October 1863 up to its end in August 1864, by hiding him in her house and escorting him to the border of Congress Poland.
Her husband, on the other hand, had been critical of the uprising from its very beginning.
He saw the uprising as untimely and highly unlikely to be successful.
He would say that the movement would only bring about unnecessary death, suffering, and grave repercussions for the nation
(Shastouski, Brykowisko, 2011).

Following the failed uprising, in December 1864, Mr. Orzeszko was arrested and sent to Russia, his land and possessions confiscated.
Deprived of her husband's estate, Eliza had no other choice than to return to her father's house in Milkowszczyzna.
Eliza has thus described her impressions from returning to her hometown:

\longquote{%
  I went out to the world in a carriage padded with blue damask and drawn by seven horses. I was accompanied by servants shining with coat of arms plaques\ldots{} At the age of twenty, I returned from those distant places all alone, to the house I have inherited from my father, only to find it looted by soldiers who had stayed in the household; [I had to pay] debts taken by other people and a tribute % kontrybucja
so enormous that even [our] fields of wheat would bend under its burden; various officials would frequently visit and overstay\ldots{} The situation was completely overwhelming\ldots{}%
\footnote{Shastouski, Orzeszkowa, 2000, pp. 7--8.}}

Eliza was not the only one to suffer in the new reality.
Following the failure of the January Uprising and the abolishment of serfdom in the Russian Empire, many families in the area of [the former Great Duchy of Lithuania] have become impoverished.
The difficult situation gave Eliza the opportunity to watch the world from the point of view of the weak and destitute, which had been crucial for her literary career.
In 1866, the young writer published her first novel, with the novel \textit{A Picture from the Years of Famine} (Polish: \textit{Obrazek z lat głodowych}), dealing with the daily struggles of the lower layers of society
(Shastouski, Orzeszkowa, 2000, p. 8).

In 1869, her marriage was annulled and she settled in Grodno (present day Hrodna, Belarus).
Since 1879, she co-owned a publishing house and bookstore in Vilnius, in present-day Lithuania.
Her most important works included \textit{The Boor} (Polish: \textit{Cham}), \textit{The Dziurdzia Family} (Polish: \textit{Dziurdziowie}), and \textit{On the Banks of the Niemen} (Polish: \textit{Nad Niemnem}).
Most of her works deal with the difficult existence of peasants, women, and impoverished gentry.
Her works are often classified as \textit{Tendenzliteratur} (German: `tendentious literature').
She had been nominated twice for the Nobel Prize in Literature.
Eliza Orzeszkowa died in Grodno in 1910 and was buried at the local Catholic cemetery
(Brykowisko 2011, Britannica).

\section{Names of Eliza Orzeszkowa}
In many Slavic languages, including Polish, many family names have traditionally taken a different form when referring to a man, his unmarried daughter, and his wife.
For instance, the wife of the well-known Polish poet of the Romanticist period, Adam Mickiewicz, was referred to as Celina Mickiewiczowa, and his eldest daughter would be referred to as Maria Mickiewiczówna, up until her marriage.
Analogously, in case of the name ``Eliza Orzeszkowa,'' Orzeszkowa means `the wife of Mr. Orzeszko,' which roughly corresponds to the English form ``Mrs. Piotr Orzeszko.''
In modern-day Polish, however, this convention is virtually obsolete.
All family members use the same form of the name, and the wife of \textit{Pan Orzeszko} (Mr. Orzeszko) can be called \textit{Pani Orzeszko} (Mrs. Orzeszko).
A notable exception to this rule are the surnames ending in \textit{-ski} or similar suffixes, such as Kowalski, Górecki, Grodzki.
These surnames have evolved from adjectives and still follow all the grammar rules pertaining to adjectives.
Thus, the wife and daughter of the Polish counterpart of John Smith, Jan Kowalski, would still use the form \textit{Kowalska}. 

In Zamenhof's translation of \textit{Marta}, the author's name is listed as Eliza Orzeszko.
Zhong Xianmin's translation follows this convention, rendering the name into Chinese with slightly distorted pronunciation as 愛麗莎・奧西斯哥 (\nazwisko{Àilìshā Àoxīsīgē}).
Other Chinese-language writings and websites use other variations, based on the traditional form ``Eliza Orzeszkowa.''
These names include 艾麗查・奧熱什科娃 (\nazwisko{Àilìchá Àorèshíkēwā}) and 艾麗查・奧若什科娃 (\nazwisko{Àilìchá Àoruòshíkēwā}). In the remaining part of this thesis, the form ``Eliza Orzeszkowa'' shall be used, being the most prevalent in Polish-language writings.

\section{Plot outline of the novel \textit{Marta}}
The novel \textit{Marta} is divided into nine unnumbered chapters and an introduction. 
The story is told from the point of view of a third-person omniscient narrator.

\textit{Marta} tells the tragical story of a certain Marta Świcka.
In the beginning of the novel, Marta is presented as an impoverished, twenty-odd-year-old lady whose affluent husband had just died, leaving her with a little daughter, no living family members, and no means of livelihood.
Due to these unfavorable circumstances, she is compelled to move out of a lavish apartment in Warsaw to a plain, dilapidated, single room.

Throughout the novel, the protagonist struggles to find a job to provide for herself and her little daughter.
At first, she tries her luck as a teacher of French and piano, but soon finds out that the little qualifications she has are not sufficient for her to make ends meet.
She also finds out that she cannot work in certain professions despite having the necessary qualifications, because those jobs are only available to men.
She manages to get a job as a seamstress, which she soon quits due to a conflict at workplace.
Her little daughter falls ill with bronchitis.
Marta is compelled to return the rudimentary furniture that she rented for her tiny apartment.
Finally, a horse-drawn omnibus runs over Marta and she dies on the spot.
